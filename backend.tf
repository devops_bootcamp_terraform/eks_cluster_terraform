terraform {
  required_version = ">=0.12.0"
  backend "s3" {
    region  = "us-east-1"
    profile = "default"
    key     = "mykeyterraformstatefile788"
    bucket  = "myterraformstatebucket8"
    dynamodb_table = "tf_state_lock"    
  }
}