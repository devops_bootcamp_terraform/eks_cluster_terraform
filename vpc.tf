locals {
  vpc_name            = "ex-${replace(basename(path.cwd), "_", "-")}"
  cidr_vpc = "10.0.0.0/16"
  azs = slice(data.aws_availability_zones.azs.names, 0, 3)
}
data "aws_availability_zones" "azs" {}
module "vpc_eks" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.19.0"
  name    = var.name
  cidr    = local.cidr_vpc
  azs = local.azs
  #private_subnets = var.private_subnets_cidr
  #public_subnets  = var.public_subnets_cidr
  private_subnets      = [for k, v in local.azs : cidrsubnet(local.cidr_vpc, 4, k)]
  public_subnets       = [for k, v in local.azs : cidrsubnet(local.cidr_vpc, 8, k + 48)]
  intra_subnets        = [for k, v in local.azs : cidrsubnet(local.cidr_vpc, 8, k + 52)]
  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_vpn_gateway   = true
  enable_dns_hostnames = true

  tags = {
    "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
    Terraform                                 = "true"
    Environment                               = var.environnement
  }
  private_subnet_tags = {
    "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
    "kubernetes.io/role/internal-elb"         = 1
  }
  public_subnet_tags = {
    "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
    "kubernetes.io/role/elb"                  = 1
  }
}



